#include <math.h>
#include <vector>
#include <queue>
#include <stack>
#include <ctype.h>
#include <stdio.h>
#include <chrono>
#include <ctime> 
#include <unordered_set>
#include <set>
#include <random>
#include <utility>
#include <random>
#include <algorithm>
#include <iostream>
using namespace std;

#define heuristic
#define DEBUG 0
//max num nodes = 1 billion for int32
#define swap2(x,y) { x = x + y; y = x - y; x = x - y; }
typedef  pair<int,int> Pair;
struct pair_hash
{
    template <class T1, class T2>
    std::size_t operator () (std::pair<T1, T2> const &pair) const
    {
        std::size_t h1 = std::hash<T1>()(pair.first);
        std::size_t h2 = std::hash<T2>()(pair.second);
        return h1 * h2 + h1 + h2 + h1 ^ h2; //if this line is poor then one may suffer for lookups in matching - I will check it today
    }
};

typedef struct Tvertex {
    int mate;
    int head;
    int uf_size;
    int uf_exact_vertice;
} Vertex;


typedef struct Tedge {
    int endpoint;
    int original_start;
    int original_end;
} Edge;

inline void make_pair(int a,int b,Pair &p){
    if (a<b){
        p.first=a;
        p.second=b;
    }else{
        p.first=b;
        p.second=a;
    }
    
}
inline void  augment_recover_graph(Edge e,vector<vector<int> > &treeedge,vector<int> &treevertex,int *treevertexdegree){
    int original_a,original_b;
    original_a=e.original_start;
    original_b=e.original_end;
    if (treevertexdegree[original_a]==0) //this is done to save some space
        treevertex.push_back(original_a);
    treevertexdegree[original_a]++;
    if (treevertexdegree[original_b]==0)
        treevertex.push_back(original_b);
    treevertexdegree[original_b]++;
    treeedge[original_b].push_back(original_a);
    treeedge[original_a].push_back(original_b);
    
}
inline void add_twins(Edge e1,Edge e2,vector<vector<int> > &twin_1,vector<vector<int> > &twin_2){
    int oe11=e1.original_start;
    int oe12=e1.original_end;
    int oe21=e2.original_start;
    int oe22=e2.original_end;
    twin_1[oe11].push_back(oe21);
    twin_2[oe11].push_back(oe22);
    
    twin_1[oe12].push_back(oe21);
    twin_2[oe12].push_back(oe22);
    
    twin_1[oe21].push_back(oe11);
    twin_2[oe21].push_back(oe12);
    
    twin_1[oe22].push_back(oe11);
    twin_2[oe22].push_back(oe12);
    
}
inline void recover_the_matching(vector<vector< int>> &treeedge,vector<int> &treevertex,int* treevertexdegree,  vector<vector<int> > &treedge_twin_1,  vector<vector<int> > &treedge_twin_2,Vertex* matching)
{ 
    int incr=0;
    stack<int> bucket1_recover;
    stack<int> bucket_matched_recover;
    for (int k=0;k < (int) treevertex.size();++k){
        if (treevertexdegree[treevertex[k]]==1){
            bucket1_recover.push(treevertex[k]);
        }
        if (matching[treevertex[k]].mate!=-1){
            bucket_matched_recover.push(treevertex[k]);
        }
    }
    while (!bucket1_recover.empty() || !bucket_matched_recover.empty())
    {
        while (!bucket_matched_recover.empty())
        {
            int v= bucket_matched_recover.top();
            int mvv=matching[v].mate;
            bucket_matched_recover.pop();
            for (int i=0;i<(int) treeedge[v].size();++i)
            {
                int u=treeedge[v][i];
                int twin_1=treedge_twin_1[v][i];
                int twin_2=treedge_twin_2[v][i];
                if (u==mvv)
                {
                    if (u>mvv){
                        treevertexdegree[twin_1]--;
                        treevertexdegree[twin_2]--;
                        if (treevertexdegree[twin_1]==1)
                            bucket1_recover.push(twin_1);
                        if (treevertexdegree[twin_2]==1)
                            bucket1_recover.push(twin_2);
                    }
                }else
                {
                    if (treevertexdegree[u]>0)
                    {
                        treevertexdegree[u]--;
                        if (treevertexdegree[u]==1)
                            bucket1_recover.push(u);
                        if (matching[twin_1].mate==-1 && matching[twin_2].mate==-1)
                        {
                            matching[twin_1].mate=twin_2;
                            matching[twin_2].mate=twin_1;
                            incr++;
                            bucket_matched_recover.push(twin_1);
                            bucket_matched_recover.push(twin_2);
                        }
                    }
                }
            }
            treevertexdegree[v]=0;
        }
        
        if (!bucket1_recover.empty())
        {
            //found deg 1 vertex
            int v = bucket1_recover.top();
            bucket1_recover.pop();
            
            if (treevertexdegree[v]>0 && matching[v].mate==-1)
            {
                int neigh=0;
                while((matching[treeedge[v][neigh]].mate!=-1) || (treevertexdegree[treeedge[v][neigh]]==0) || (matching[treedge_twin_1[v][neigh]].mate==treedge_twin_2[v][neigh]))
                {
                    neigh++;
                    if(neigh>=(int) treeedge[v].size())
                        break;
                }
                if(neigh>=(int) treeedge[v].size()){
                    printf("this shouldnt happen!!! \n");
                    break;
                    
                }
                
                //assert this
                int  u=treeedge[v][neigh];
                matching[v].mate=u;
                matching[u].mate=v;
                incr++;
                bucket_matched_recover.push(u);
                bucket_matched_recover.push(v);
                
                treevertexdegree[v]=0;
            }
        }
    }
    int both=0;
    int only_one=0;
    int none=0;
    int could_extend=0;
    for (int i=0;i<(int) treevertex.size();++i){
        int u=treevertex[i];
        for (int j=0;j<(int) treeedge[u].size();++j){
            int twin_1=treedge_twin_1[u][j];
            int twin_2=treedge_twin_2[u][j];
            int v=treeedge[u][j];
            if(matching[v].mate==-1 && matching[u].mate==-1){
                could_extend++;
                
            }
            if (matching[v].mate==u){
                if (matching[twin_1].mate==twin_2){
                    both++;
                }else {
                    only_one++;
                }
            }else{
                if (matching[twin_1].mate==twin_2){
                    only_one++;
                }else {
                    none++;
                }
            }
        }
    }
}
inline void remove_element(vector<Edge> &vec,int element)
{
    vec[element]=vec.back();
    vec.pop_back();
}

inline void remove_element_int(vector<int> &vec,int element)
{
    vec[element]=vec.back();
    vec.pop_back();
}

inline void reduce_degree (int i,int* remdegree,stack<int> &bucket1,stack<int> &bucket2)
{
    remdegree[i]--;
    if(remdegree[i]==1)
        bucket1.push(i);
    else
        if(remdegree[i]==2)
            bucket2.push(i);
}
inline void reduce_degree_KS1 (int i,int* remdegree,stack<int> &bucket1)
{
    remdegree[i]--;
    if(remdegree[i]==1)
        bucket1.push(i);
    
}
inline void remove_neighborhood(int u,vector<Edge> &edge,int* remdegree,stack<int> &bucket1,stack<int> &bucket2)
{
    int examined=0;
    int i=0;
    while (examined<remdegree[u]-1){
        if (remdegree[edge[i].endpoint]){
            reduce_degree(edge[i].endpoint,remdegree,bucket1,bucket2);
            examined++;
        }
        i++;
    }
    remdegree[u]=0;
    edge.clear();
}
inline void init_undirected(vector<vector<Edge> > &edge,stack<int> &bucket1, stack<int> &bucket2,int *remdegree,int *row_ptrs,int *row_ids2,int n,int *ignore){
    int rd=0;
    Edge tmpedge;
    
    for(int i=0;i<n;i++)
    {
        remdegree[i]=row_ptrs[i+1]-row_ptrs[i];
        ignore[i]=0;
        edge[i].resize(remdegree[i]); rd=0;
        for (int j=row_ptrs[i];j<row_ptrs[i+1];++j)
        {
            tmpedge.endpoint=row_ids2[j];
            tmpedge.original_start=i;
            tmpedge.original_end=row_ids2[j];
            edge[i][rd++]=tmpedge;
        }
        if(remdegree[i]==1) bucket1.push(i);
        else
            if(remdegree[i]==2) bucket2.push(i);
    }
}
inline void init_bipartite(vector<vector<Edge> > &edge,stack<int> &bucket1, stack<int> &bucket2,int *remdegree,int *row_ptrs,int *col_ptrs,int *row_ids2,int *col_ids2,int n,int m,int *ignore){
    int rd=0;
    Edge tmpedge;
    
    for(int i=0;i<n;i++)
    {
        remdegree[i]=row_ptrs[i+1]-row_ptrs[i];
        ignore[i]=0;
        edge[i].resize(remdegree[i]); rd=0;
        for (int j=row_ptrs[i];j<row_ptrs[i+1];++j)
        {
            
            tmpedge.endpoint=row_ids2[j]+n;
            tmpedge.original_start=i;
            tmpedge.original_end=row_ids2[j]+n;
            edge[i][rd++]=tmpedge;
        }
        if(remdegree[i]==1) bucket1.push(i);
        else
            if(remdegree[i]==2) bucket2.push(i);
    }
    //copy cols
    for(int i=0;i<m;i++)
    {
        ignore[n+i]=0;
        remdegree[n+i]=col_ptrs[i+1]-col_ptrs[i];
        edge[n+i].resize(remdegree[n+i]); rd=0;
        for (int j=col_ptrs[i];j<col_ptrs[i+1];++j)
        {
            tmpedge.endpoint=col_ids2[j];
            tmpedge.original_start=i+n;
            tmpedge.original_end=col_ids2[j];
            edge[i+n][rd++]=tmpedge;
        }
        if(remdegree[i+n]==1) bucket1.push(i+n);
        else
            if(remdegree[i+n]==2) bucket2.push(i+n);
    }
}
inline void init_KS1_ug(vector<vector<Edge> > &edge,stack<int> &bucket1, int *remdegree,int *row_ptrs,int *row_ids2,int n){
    int rd=0;
    Edge tmpedge;
    for(int i=0;i<n;i++)
    {
        remdegree[i]=row_ptrs[i+1]-row_ptrs[i];
        edge[i].resize(remdegree[i]); rd=0;
        for (int j=row_ptrs[i];j<row_ptrs[i+1];++j)
        {
            tmpedge.endpoint=row_ids2[j];
            tmpedge.original_start=i;
            tmpedge.original_end=row_ids2[j];
            edge[i][rd++]=tmpedge;
        }
        if(remdegree[i]==1) bucket1.push(i);
    }
    return;
}
inline void init_KS1(vector<vector<Edge> > &edge,stack<int> &bucket1, int *remdegree,int *row_ptrs,int *col_ptrs,int *row_ids2,int *col_ids2,int n,int m){
    int rd=0;
    Edge tmpedge;
    for(int i=0;i<n;i++)
    {
        remdegree[i]=row_ptrs[i+1]-row_ptrs[i];
        edge[i].resize(remdegree[i]); rd=0;
        for (int j=row_ptrs[i];j<row_ptrs[i+1];++j)
        {
            
            tmpedge.endpoint=row_ids2[j]+n;
            tmpedge.original_start=i;
            tmpedge.original_end=row_ids2[j]+n;
            edge[i][rd++]=tmpedge;
        }
        if(remdegree[i]==1) bucket1.push(i);
    }
    //copy cols
    for(int i=0;i<m;i++)
    {
        remdegree[n+i]=col_ptrs[i+1]-col_ptrs[i];
        
        edge[n+i].resize(remdegree[n+i]); rd=0;
        
        for (int j=col_ptrs[i];j<col_ptrs[i+1];++j)
        {
            tmpedge.endpoint=col_ids2[j];
            tmpedge.original_start=i+n;
            tmpedge.original_end=col_ids2[j];
            edge[i+n][rd++]=tmpedge;
        }
        if(remdegree[i+n]==1) bucket1.push(i+n);   
    }   return;
}
inline void shuffle_perm(int *rand_perm,int nov,int my_seed){
    minstd_rand simple_rand;
    simple_rand.seed(my_seed);
    
    shuffle(&rand_perm[0],&rand_perm[nov-1],simple_rand);
}
inline void shuffle_list(int **rand_edge_list,int nnz,int my_seed){
    minstd_rand simple_rand;
    simple_rand.seed(my_seed);
    shuffle(&rand_edge_list[0],&rand_edge_list[nnz-1],simple_rand);
}

