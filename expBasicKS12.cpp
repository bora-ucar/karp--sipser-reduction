#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <iostream>
#include <fstream>
#include "utils.h"
#include "graph.h"

void KS2basic(int* col_ptrs, int* col_ids2, int* row_ptrs, int* row_ids2,
              int* match, int* row_match, int n, int m, double& init_time,
              double& kernel_time, double& ks_time,double &recover_time,
              int** rand_edge_list,int is_undirected);

char mfile[2048] = {'\0'};
int inputType = 0;/*by default assumed to be bipartite*/
int printMatching = 0; /*by default we do not print matching*/
char outfile[2048] = {'\0'};

void help() 
{
    printf("usage: expBasicKS12\n");
    printf("\t -h: help\n");
    printf("\t -f filename: matrix filename\n");
    printf("\t -o 0 | 1: write matching into the file filename_mtch (0--no, by default)\n");
    printf("\t -i 0 | 1: input as bipartite graph (0) or general undirected (1), default = 0]\n");
}

int parse_args(int argc, char *argv[]) 
{
    int opt;
    int mfile_provided = 0;
    
    while((opt = getopt(argc, argv, "f:i:o:h")) != -1)
    {
        switch(opt)
        {
            case 'f':
                strcpy(mfile, optarg);
                mfile_provided = 1;
                break;
            case 'i':
                inputType = atoi(optarg);
                break;
            case 'o':
                printMatching = atoi(optarg);
                break;
            case 'h':
            case '?':
                help();
                return 0;
            default:
                return 0;
        }
    }
    
    if(inputType > 2 || inputType < 0) {
        printf("wrong input type (need 0 or 1)\n");
        help();
        return 0;
    }
    
    if(!mfile_provided){
        printf("Matrix file is not provided\n");
        help();
        return 0;
    }
    return 1;
}

void reorder(int* cxadj, int* cadj, int* rxadj, int* radj, int c_nov, int r_nov, int **rand_edge_list, 
             int* r_perm, int* c_perm, int *r_degs, int *c_degs, int is_undirected)
{
    
    
    for (int i=0;i<r_nov;++i)
        r_degs[r_perm[i]]=rxadj[i+1]-rxadj[i];
    for (int i=0;i<c_nov;++i)
        c_degs[c_perm[i]]=cxadj[i+1]-cxadj[i];
    
    rxadj[0]=0;
    for (int i=1;i<=r_nov;++i)
    {
        rxadj[i]=rxadj[i-1] + r_degs[i-1];
        r_degs[i-1]=rxadj[i]-1;
    }
    cxadj[0]=0;
    for (int i=1;i<=c_nov;++i)
    {
        cxadj[i]=cxadj[i-1]+c_degs[i-1];
        c_degs[i-1]=cxadj[i]-1;
    }
    
    int nnz=rxadj[r_nov];
    int edges = nnz;
    if(is_undirected)
        edges /=2;
    int u,v,ru,rv;
    
    for (int i=0;i<edges;++i)
    {
        u=rand_edge_list[i][0];
        
        v=rand_edge_list[i][1]-r_nov * (1-is_undirected);
        ru=r_perm[u];
        rv=c_perm[v];
        radj[r_degs[ru]--]=rv;
        cadj[c_degs[rv]--]=ru;
        if(is_undirected)
        {
            cadj[c_degs[ru]--]=rv;
            radj[r_degs[rv]--]=ru;
        }
        
        rand_edge_list[i][0]=ru;
        rand_edge_list[i][1]=rv+r_nov * (1-is_undirected);
        if ( rand_edge_list[i][0] > rand_edge_list[i][1])
        {
            int t = rand_edge_list[i][0] ;
            rand_edge_list[i][0] = rand_edge_list[i][1];
            rand_edge_list[i][1] = t;
        }
    }
    
    return;
}
int exp(char *mfile,int* cxadj, int* cadj, int* rxadj, int* radj, int c_nov, int r_nov, int is_undirected) {
    int nnz=cxadj[c_nov];
    int edges=nnz;
    if (is_undirected) edges/=2;
    int **rand_edge_list=(int**)malloc(edges*sizeof(int*));
    int* rmatch = (int*) malloc(sizeof(int) * (r_nov));
    int* cmatch = (int*) malloc(sizeof(int) * (c_nov));
    int *r_perm=(int*)malloc(sizeof(int) * r_nov);
    int *c_perm=(int*)malloc(sizeof(int) *c_nov);
    int *r_degs=(int*)malloc(sizeof(int) * r_nov);
    int *c_degs=(int*)malloc(sizeof(int) *c_nov);
    int* temp_rmatch = (int*) malloc(sizeof(int) * (r_nov));
    int* temp_cmatch = (int*) malloc(sizeof(int) * (c_nov));
    int edge_num=0;
    ofstream outf;
    
    for (int i=0;i<r_nov;++i){
        for (int j=rxadj[i];j<rxadj[i+1];++j){
            if (!is_undirected || (is_undirected && i<radj[j]))
            {
                rand_edge_list[edge_num]=(int*)malloc(2*sizeof(int));
                rand_edge_list[edge_num][0]=i;
                rand_edge_list[edge_num++][1]=radj[j]+r_nov*(1-is_undirected);
            }
        }
    }
    shuffle_list(rand_edge_list, edges, 1);
    
    for (int i=0;i<r_nov;++i)
        r_perm[i]=i;
    if(is_undirected == 0)
    {
        for (int i=0;i<c_nov;++i)
            c_perm[i]=i;
    }
    
    shuffle_perm(r_perm, r_nov, 1);
    if(is_undirected == 0)
        shuffle_perm(c_perm,c_nov, 1);
    
    if(is_undirected == 0)
        reorder(cxadj, cadj, rxadj, radj, c_nov, r_nov,rand_edge_list,r_perm, c_perm, r_degs, c_degs, is_undirected);
    else
        reorder(cxadj, cadj, rxadj, radj, c_nov, r_nov,rand_edge_list,r_perm, r_perm, r_degs, c_degs, is_undirected);
    
    double init_time,kern_time,ks_time,rec_time;
    
    KS2basic(cxadj, cadj, rxadj, radj, cmatch,  rmatch, r_nov,  c_nov,init_time,kern_time,ks_time,rec_time, rand_edge_list, is_undirected);
    
    int matches=0;
    int *inv_rperm = (int *) malloc(sizeof(int) * r_nov);
    int *inv_cperm =(int *) NULL;
    
    if(is_undirected == 0)
        inv_cperm = (int *) malloc(sizeof(int) * c_nov);
    
    for (int i=0;i < r_nov; ++i)
        inv_rperm [r_perm[i]] = i;
    
    if(is_undirected == 0)
    {
        for (int i=0; i < c_nov; ++i)
            inv_cperm [c_perm[i]] = i;
    }
    
    if(printMatching)
    {
        int tt = snprintf(outfile, 2047, "%s_mtch", mfile);
        if(tt < 0 || tt >= 2047)
        {
            cerr <<"expBasicKS12: An error occured while writing output file name"<<endl;
            std::terminate();
        }
        
        outf.open (outfile);
        for (int i=0; i < r_nov; ++i)
        {
            if (rmatch[i]!=-1)
            {
                int r = inv_rperm[i]+1;
                int mr ;
                if(is_undirected)
                    mr = inv_rperm [rmatch[i]]+1;
                else
                    mr = inv_cperm [rmatch[i]]+1;
                outf << r << " "<< mr <<endl;
                matches++;
            }
        }
        outf.close();
    }
    else
    {
        for (int i=0;i<r_nov;++i)
        {
            if (rmatch[i]!=-1)
                matches++;
        }
    }
    
    if (is_undirected) matches/=2;
    
    printf("matches: %d\n",matches);
    
    if(is_undirected == 0)
        free(inv_cperm);
    free(inv_rperm);
    free(r_perm);
    free(c_perm);
    free(r_degs);
    free(c_degs);
    free(temp_cmatch);
    free(temp_rmatch);
    free(rand_edge_list);
    free(rmatch);
    free(cmatch);
    
    return 0;
}

int main(int argc, char *argv[]) {
    int *rxadj, *cxadj, *radj, *cadj, i, r_nov, r_org, c_nov, ptr, nz, nbr;
    double *tmp1, *tmp2;
    
    int ireadType;
    
    if(!parse_args(argc,argv))
        return -1;
    
    
    fflush(0);
    int is_undirected = inputType;
    ireadType = read_graph(mfile, &rxadj, &radj, &tmp1, &tmp2, &r_nov, 1);
    r_org = r_nov;

    if( ireadType == -1) {
        cerr<<"error in reading the matrix";
        std::terminate();
    }
    
    if(inputType == 1 && ireadType != 1)
    {
        printf("expBasicsKS12: error in usage. inputType is graph, but the matrix in the file %s is not symmetric\n", mfile);
        exit(1);
    }
    nz = rxadj[r_nov];
    
    //get the transpose/reverse graph of the graph
    //this loop finds the maximum cnov id
    c_nov = 0;
    for(i = 0; i < r_nov; i++)
    {
        for(ptr = rxadj[i]; ptr < rxadj[i+1]; ptr++)
        {
            nbr = radj[ptr];
            if(nbr > c_nov) c_nov = nbr;
        }
    }
    c_nov = c_nov + 1;
    
    cxadj = (int*) malloc(sizeof(int) * (c_nov + 1));
    memset(cxadj, 0, sizeof(int) * (c_nov + 1));
    
    //this loop finds the degrees
    cadj = (int*) malloc(sizeof(int) * nz);
    for(i = 0; i < r_nov; i++)
    {
        for(ptr = rxadj[i]; ptr < rxadj[i+1]; ptr++)
        {
            nbr = radj[ptr];
            cxadj[nbr + 1]++;
        }
    }
    
    for(i = 1; i <= c_nov; i++)
        cxadj[i] += cxadj[i-1];    
    
    for(i = 0; i < r_nov; i++)
    {
        for(ptr = rxadj[i]; ptr < rxadj[i+1]; ptr++)
        {
            nbr = radj[ptr];
            cadj[cxadj[nbr]] = i;
            cxadj[nbr]++;
        }
    }
    
    for(i = c_nov; i > 0; i--) {
        cxadj[i] = cxadj[i-1];
    }
    cxadj[0] = 0;
    
    //remove empty rows / columns
    int rno = 0;
    int* rids = (int*) malloc(sizeof(int) * r_nov);
    for(i = 0; i < r_nov; i++)
    {
        rids[i] = -1;
        if(rxadj[i] != rxadj[i+1])
        {
            rxadj[rno + 1] = rxadj[rno]+ (rxadj[i+1] - rxadj[i]);
            rids[i] = rno++;
        }
    }
    rxadj[rno] = rxadj[r_nov];
    r_nov = rno;
    
    for(ptr = 0; ptr < cxadj[c_nov]; ptr++)
        cadj[ptr] = rids[cadj[ptr]];
    
    free(rids);
    
    int cno = 0;
    int* cids = (int*) malloc(sizeof(int) * c_nov);
    for(i = 0; i < c_nov; i++)
    {
        cids[i] = -1;
        if(cxadj[i] != cxadj[i+1])
        {
            cxadj[cno + 1] = cxadj[cno]+ (cxadj[i+1] - cxadj[i]);
            cids[i] = cno++;
        }
    }
    c_nov = cno;
    for(ptr = 0; ptr < rxadj[r_nov]; ptr++)
        radj[ptr] = cids[radj[ptr]];
    
    free(cids);
    if (is_undirected)
    { /* zero out the diagonal*/
        int diagonal_edges=0;
        for (i=0;i<r_nov;++i)
        {
            for (ptr=rxadj[i];ptr<rxadj[i+1];++ptr)
            {
                if (radj[ptr]==i){
                    diagonal_edges++;
                    break;
                }
            }
        }
        if (diagonal_edges !=0 )
        {
            int *trxadj, *tradj;
            trxadj=(int*)malloc(sizeof(int)*(1+r_nov));
            tradj=(int*)malloc(sizeof(int)*(nz-diagonal_edges));
            int curr=0;
            for (i=0;i<r_nov;++i){
                trxadj[i]=curr;
                for (ptr=rxadj[i];ptr<rxadj[i+1];++ptr){
                    if (radj[ptr]!=i){
                        tradj[curr++]=radj[ptr];
                    }
                }
            }
            trxadj[r_nov]=curr;
            exp(mfile, trxadj, tradj, trxadj, tradj, r_nov, r_nov, 1);
        }
        else
        {
            exp(mfile, rxadj, radj, rxadj, radj, r_nov, r_nov, 1);
        }
    }
    else/*bipartite graph*/
    {
        exp(mfile,cxadj, cadj, rxadj, radj, c_nov, r_nov, 0);
    }
    if(r_nov != r_org || c_nov != r_org)
        cout << "compare nrows and ncols " << r_nov << " "<< c_nov <<" with before \n\t(if not the same; there are zero degree rows/cols which are discarded)"<<endl;
    return 0;
}

