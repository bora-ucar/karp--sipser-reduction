#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/stat.h>
#include "mmio.h"
#include "graph.h"

typedef struct {
    vtype i;
    vtype j;
    ewtype w;
} Triple;

int cmp(const void *a, const void *b) 
{
    const vtype *ia = (const vtype *)a;
    const vtype *ib = (const vtype *)b;
    return *ia - *ib;
}

int tricmp(const void *t1, const void *t2) 
{
    Triple *tr1 = (Triple *)t1;
    Triple *tr2 = (Triple *)t2;
    if (tr1->i == tr2->i) {
        return (int)(tr1->j - tr2->j);
    }
    return (int)(tr1->i - tr2->i);
}

int read_mtx(FILE* fp, etype **pxadj, vtype **padj,
             ewtype **pewghts, vwtype **pvwghts,
             vtype* pnov, int loop, int zerobased)
{
    Triple *T;
    vtype i, j, M, N, lower, upper_N, upper_M, wi;
    ewtype w;
    etype k, ecnt, cnt;
    long long int nz;
    MM_typecode matcode;
    
    if (mm_read_banner(fp, &matcode) != 0)
    {
        printf("Could not process Matrix Market banner.\n");
        return -1;
    }
    
    mm_read_mtx_crd_size(fp, &M, &N, &nz);
    printf("There are %ld rows, %ld columns, %ld nonzeros in the file\n", (long int)M, (long int)N, (long int)nz);
    
    lower = 1 - zerobased;
    upper_M = M - zerobased;
    upper_N = N - zerobased;
    
    unsigned long long sizeNeeded = 2 * nz * sizeof(Triple);
    T = (Triple *)malloc(sizeNeeded);
    cnt = 0;
    if (mm_is_pattern(matcode))
    {
        for (ecnt = 0; ecnt < nz; ecnt++)
        {
            fscanf(fp, "%d %d\n", &i, &j);
            
            if (i < lower || j < lower || i > upper_M || j > upper_N)
            {
                printf("read coordinate %ld %ld -- lower and uppers is %ld and %ld-%ld\n", (long int)i, (long int)j, (long int)lower, (long int)upper_M, (long int)upper_N);
                return -1;
            }
            
            if (loop || i != j)
            {
                T[cnt].i = i;
                T[cnt].j = j;
                T[cnt].w = 1;
                cnt++;
                
                if (mm_is_symmetric(matcode) && i != j)
                { //mm_is_symmetric(matcode)
                    T[cnt].i = T[cnt - 1].j;  /* insert the symmetric edge */
                    T[cnt].j = T[cnt - 1].i;
                    T[cnt].w = 1;
                    cnt++;
                }
            }
        }
    }
    else {
        for (ecnt = 0; ecnt < nz; ecnt++)
        {
            if (mm_is_real(matcode))
            {
                fscanf(fp, "%d %d %lf\n", &i, &j, &w);
            }
            else if (mm_is_integer(matcode))
            {
                fscanf(fp, "%d %d %d\n", &i, &j, &wi);
            }
            w = (double)wi;
            
            if (i < lower || j < lower || i > upper_M || j > upper_N)
            {
                printf("read coordinate %ld %ld -- lower and uppers is %ld and %ld-%ld\n", (long int)i, (long int)j, (long int)lower, (long int)upper_M, (long int)upper_N);
                return -1;
            }
            
            //      printf("%d: %d %d %f\n", cnt, i, j, w);
            if (loop || i != j)
            {
                T[cnt].i = i;
                T[cnt].j = j;
                T[cnt].w = fabs(w);
                cnt++;
                
                if (mm_is_symmetric(matcode) && i != j)
                { /* insert the symmetric edge */
                    T[cnt].i = T[cnt - 1].j;
                    T[cnt].j = T[cnt - 1].i;
                    T[cnt].w = T[cnt - 1].w;
                    cnt++;
                }
            }
        }
    }
    
    /* eliminate the duplicates, and writes them to the arrays */
    qsort(T, cnt, sizeof(Triple), tricmp);
    
    /* create xadj array */
    (*pxadj) = (etype*)malloc(sizeof(etype) * (M + 1));
    memset((*pxadj), 0, sizeof(etype) * (M + 1));
    
    k = 0;
    (*pxadj)[T[0].i + zerobased]++;
    for (ecnt = 1; ecnt < cnt; ecnt++)
    {
        i = T[ecnt].i;
        if (i != T[ecnt - 1].i || T[ecnt].j != T[ecnt - 1].j)
        { /* if this edge is not the same as previous one */
            (*pxadj)[i + zerobased]++;
            k = i; /* the first edge entry */
        }
        else { /* add the weight to the original */
            T[k].w += T[i].w; /* add the weight to the representative */
        }
    }
    for (i = 2; i <= M; i++) (*pxadj)[i] += (*pxadj)[i - 1];
    
    (*padj) = (vtype*)malloc(sizeof(vtype) * (*pxadj)[M]);
    (*pewghts) = (ewtype*)malloc(sizeof(ewtype) * (*pxadj)[M]);
    (*padj)[0] = T[0].j - 1 + zerobased; (*pewghts)[0] = T[0].w; k = 1;
    
    for (ecnt = 1; ecnt < cnt; ecnt++)
    {
        i = T[ecnt].i;
        if (i != T[ecnt - 1].i || T[ecnt].j != T[ecnt - 1].j)
        { /* if this edge is not the same as previous one */
            (*padj)[k] = T[ecnt].j - 1 + zerobased; /* adjust from 1-based to 0-based */
            (*pewghts)[k++] = T[ecnt].w;
        }
    }
    
    (*pvwghts) = (vwtype*)malloc(sizeof(vwtype) * M);
    for (i = 0; i < M; i++) (*pvwghts)[i] = 1;
    
    *pnov = M; // the number of vertices
    
    free(T);
    return mm_is_symmetric(matcode) ;
}


void graph_info(int* xadj, int* adj, int nov) 
{
    const int mxdeg = 5;
    int degs[mxdeg];
    memset(degs, 0, sizeof(int) * mxdeg);
    double avg_deg = (xadj[nov] + .0f) / nov;
    double stdev = 0;
    int N = 0;
    int i;
    for(i = 0; i < nov; i++) {
        int deg = xadj[i+1] - xadj[i];
        if(deg < mxdeg) degs[deg]++;
        stdev += (deg - avg_deg) * (deg - avg_deg);
        
        long long ptr;
        for(ptr = xadj[i]; ptr < xadj[i+1]; ptr++)
        {
            int nbr = adj[ptr];
            if(nbr > N) N = nbr;
        }
    }
    N = N + 1;
    
    stdev = sqrt(stdev/ nov);
    
    /*
    printf("M, N, E -  %d %d %d\n", nov, N, xadj[nov]);
    printf("Average deg: %.2f\n", avg_deg);
    printf("Degree std : %.2f\n", stdev);
    printf("#deg [0,%d]: ", mxdeg - 1);
    for(i = 0; i < mxdeg; i++)
        printf("%d ", degs[i]);

    
    printf("\n");
    */
}

int read_graph(char* gfile, etype **xadj, vtype **adj,
               ewtype **ewghts, vwtype **vwghts,
               vtype* nov, int loop)
{
    FILE *fp;
    int ireadType = 0;
    /* read from text, write to binary */
    printf("Reading from %s\n", gfile);
    fp = fopen(gfile, "r");
    if (fp == NULL)
    {
        printf("%s: file does not exist\n", gfile);
        return -1;
    }
    else
    {
        ireadType = read_mtx(fp, xadj, adj, ewghts, vwghts, nov, loop, 0);
        if ( ireadType == -1)
        {
            printf("problem with mtx file\n");
            fclose(fp);
            return -1;
        }
        fclose(fp);
    }
    
    puts("----------------------------------");
    graph_info(*xadj, *adj, *nov);
    
    return ireadType;
}
