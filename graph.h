#ifdef __cplusplus
extern "C" {
#endif

#ifndef GRAPH_H
#define GRAPH_H

#define vtype int
#define etype int
#define ewtype double
#define vwtype double

int read_graph(char* gfile, etype **xadj, vtype **adj,
               ewtype **ewghts, vwtype **vwghts,
               vtype* nov, int loop);
#endif

#ifdef __cplusplus
}
#endif
