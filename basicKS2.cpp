#include <math.h>
#include <vector>
#include <queue>
#include <stack>
#include <ctype.h>
#include <stdio.h>
#include <chrono>
#include <ctime> 
#include <unordered_set>
#include <set>
#include <iostream>
#include "utils.h"
#include "graph.h"

using namespace std;

//max num nodes = 1 billion for int32


inline void basic_remove_neighborhood(int u,vector<Edge> &edge,int* remdegree,stack<int> &bucket1,stack<int> &bucket2)
{
    int examined=0;
    int i=0;
    while (examined < remdegree[u]-1){
        if (remdegree[edge[i].endpoint]){
            examined++;
            reduce_degree(edge[i].endpoint,remdegree,bucket1,bucket2);
        }
        i++;
    }
    remdegree[u]=0;
    edge.clear();
}
//High-quality matching heuristic
void KS2basic(int* col_ptrs, int* col_ids2, int* row_ptrs, int* row_ids2,
              int* match, int* row_match, int n, int m, double& init_time,
              double& kernel_time, double& ks_time, double &recover_time,
              int** rand_edge_list, int is_undirected)
{
    int NSIZE=n + (1-is_undirected)*m;
    
    auto start = std::chrono::system_clock::now();
    int u,w,ufu,ufw,uf1,uf2;
    stack<int> bucket1;
    stack<int> bucket2;
    vector<vector<Edge> > edge;
    vector<vector<int> > treeedge;
    vector<int> treevertex;
    vector<vector<int> > treedge_twin_1;
    vector<vector<int> >treedge_twin_2;
    Edge tmpedge;
    int* ignore=(int*)malloc((NSIZE)*sizeof(int));
    int remain=0;
    int deg1count =0;
    int deg2count =0;
    int nnz=row_ptrs[n];
    int next;
    int edges=nnz;
    if (is_undirected) edges/=2;
    //  int** rand_edge_list=(int**)malloc(nnz*sizeof(int*));
    Vertex* matching = (Vertex*)calloc(NSIZE,sizeof(Vertex));
    int* remdegree = (int*)malloc((NSIZE)*sizeof(int));
    int* treevertexdegree = (int*)calloc(NSIZE,sizeof(int));
    edge.resize(NSIZE);
    treeedge.resize(NSIZE);
    treedge_twin_1.resize(NSIZE);
    treedge_twin_2.resize(NSIZE);    
    
    for(int i=0;i<NSIZE;i++)
    {
        matching[i].mate=-1;
        matching[i].head=i;
        matching[i].uf_size=1;
        matching[i].uf_exact_vertice=i;
    }
    int rcc=0;
    
    int* mergearray;
    mergearray = (int*)calloc(NSIZE,sizeof(int));
    // printf("use merge array \n");
    
    if (is_undirected)
        init_undirected(edge,bucket1, bucket2,remdegree,row_ptrs,row_ids2,n,ignore);
    else
        init_bipartite(edge,bucket1, bucket2,remdegree,row_ptrs,col_ptrs,row_ids2,col_ids2,n,m,ignore);
    
    auto el1 = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds= el1-start;
    init_time=elapsed_seconds.count();
    start = std::chrono::system_clock::now();
    
    //main loop
    while((remain<edges) || !bucket1.empty() || !bucket2.empty())
    {
        //Deg 1 vertices
        if(!bucket1.empty())
        {
            int v=bucket1.top();
            bucket1.pop();
            if (remdegree[v]==1)
            {
                deg1count++;
                int point=0;
                while (!remdegree[edge[v][point].endpoint])
                    point++;
                u=edge[v][point].endpoint;
                matching[edge[v][point].original_start].mate=edge[v][point].original_end;
                matching[edge[v][point].original_end].mate=edge[v][point].original_start;
                ignore[edge[v][point].original_start]=1;
                ignore[edge[v][point].original_end]=1;
                ignore[u]=1;
                ignore[v]=1;
                remdegree[v]=0;
                
                basic_remove_neighborhood(u,edge[u],remdegree,bucket1,bucket2);
                edge[v].clear();
            }
        }
        else
        {
            if(!bucket2.empty())
            {
                int v = bucket2.top();
                bucket2.pop();
                if(remdegree[v]==2)
                {
                    int point=0;
                    while(!remdegree[edge[v][point].endpoint])
                        point++;
                    int u=edge[v][point].endpoint;
                    Edge e1=edge[v][point];
                    point++;
                    while(!remdegree[edge[v][point].endpoint])
                        point++;
                    int w=edge[v][point].endpoint;
                    Edge e2=edge[v][point];
                    if(remdegree[w]>remdegree[u])
                        swap2(u,w);
                    ufw=w;
                    while (matching[ufw].head != ufw)
                    {
                        next=matching[ufw].head;
                        matching[ufw].head=matching[next].head;
                        ufw=next;
                    }
                    ufu=u;
                    while (matching[ufu].head != ufu)
                    {
                        next=matching[ufu].head;
                        matching[ufu].head=matching[next].head;
                        ufu=next;
                    }
                    if (matching[ufu].uf_size >= matching[ufw].uf_size)
                    {
                        matching[ufw].head=ufu;  //head of ufu points already to u
                        matching[ufu].uf_size+=matching[ufw].uf_size;
                    }
                    else
                    {
                        matching[ufu].head=ufw;
                        matching[ufw].uf_size+=matching[ufu].uf_size;
                        matching[ufw].uf_exact_vertice=u; //now we need this
                    }
                    augment_recover_graph(e1,treeedge,treevertex,treevertexdegree);
                    augment_recover_graph(e2,treeedge,treevertex,treevertexdegree);
                    add_twins(e1,e2,treedge_twin_1,treedge_twin_2);
                    remdegree[v]=0;
                    int i=0;
                    
                    int upper_limit=edge[u].size()-1;
                    for (int i=upper_limit;i>=0;--i)
                    {
                        if (remdegree[edge[u][i].endpoint])
                        {
                            if (edge[u][i].endpoint!=w){
                                
                                mergearray[edge[u][i].endpoint]=u;//mergecount;
                            }else{
                                remove_element(edge[u],i);
                                
                            }
                        }
                        else
                            remove_element(edge[u],i);
                    }
                    
                    remdegree[w]--;
                    int examined=0;
                    i=0;
                    while (examined<remdegree[w])
                    {
                        if (remdegree[edge[w][i].endpoint] && edge[w][i].endpoint!=u)
                        {
                            
                            if(mergearray[edge[w][i].endpoint]!=u)//mergecount)
                            {
                                edge[u].push_back(edge[w][i]);
                                tmpedge.endpoint=u;
                                tmpedge.original_start=edge[w][i].original_end;
                                tmpedge.original_end=edge[w][i].original_start;
                                
                                edge[edge[w][i].endpoint].push_back(tmpedge);
                                remdegree[u]++;
                            }
                            else
                            {
                                reduce_degree(edge[w][i].endpoint,remdegree,bucket1,bucket2);
                            }
                            examined++;
                            
                        }
                        else if (edge[w][i].endpoint==u)
                        {
                            remdegree[u]--;
                            examined++;
                        }
                        i++;
                    }
                    remdegree[w]=0;
                    deg2count++;
                    reduce_degree(u,remdegree,bucket1,bucket2);
                    edge[v].clear();
                    edge[w].clear();
                }
            }
            else
            {
                if (rcc==0)
                {
                    auto el2 = std::chrono::system_clock::now();
                    std::chrono::duration<double> ela = el2-start;
                    kernel_time=ela.count();
                }
                while (remain < edges) 		//Deg 3+ vertices
                {
                    u=rand_edge_list[remain][0];
                    w=rand_edge_list[remain][1];
                    ufu=u;
                    ufw=w;
                    if (!ignore[u] && !ignore[w])
                    {                        
                        while(matching[ufu].head!=ufu && !ignore[ufu])
                        {
                            next=matching[matching[ufu].head].head;
                            matching[ufu].head=next;
                            ufu=next;
                        }
                        uf1=ufu;
                        ufu=matching[ufu].uf_exact_vertice;
                        if (!ignore[ufu] && !ignore[uf1])
                        {
                            while(matching[ufw].head!=ufw && !ignore[ufw])
                            {
                                next=matching[matching[ufw].head].head;
                                matching[ufw].head=next;
                                ufw=next;
                            }
                            uf2=ufw;
                            ufw=matching[ufw].uf_exact_vertice;
                            if (!ignore[ufw] && !ignore[uf2])
                            {
                                if (remdegree[ufu] && remdegree[ufw])
                                {
                                    rcc++;
                                    matching[u].mate=w;matching[w].mate=u;
                                    ignore[u]=1;ignore[w]=1;
                                    ignore[ufu]=1; ignore[ufw]=1;
                                    ignore[uf1]=1; ignore[uf2]=1;
                                    int od=remdegree[ufu];
                                    remdegree[ufu]=0;
                                    
                                    basic_remove_neighborhood(ufw,edge[ufw],remdegree,bucket1,bucket2);
                                    remdegree[ufu]=od;
                                    basic_remove_neighborhood(ufu,edge[ufu],remdegree,bucket1,bucket2);
                                    edge[ufw].clear();
                                    edge[ufu].clear();
                                    remain++;
                                    break;
                                }
                                else
                                {
                                    if (!remdegree[ufu])
                                    {
                                        ignore[u]=1;
                                        ignore[ufu]=1;
                                        ignore[uf1]=1;
                                    }
                                    if (!remdegree[ufw])
                                    {
                                        ignore[w]=1;
                                        ignore[ufw]=1;
                                        ignore[uf2]=1;
                                    }
                                }
                            }
                            else
                            {
                                ignore[ufw]=1;
                                ignore[uf2]=1;
                            }
                        }
                        else
                        {
                            ignore[ufu]=1;
                            ignore[uf1]=1;
                        }
                    }
                    remain++;
                }
            }
        } 
    }
    
    auto split = std::chrono::system_clock::now();
    elapsed_seconds = split-start;
    std::cout <<  "split time: " << elapsed_seconds.count() << "s\n";
    ks_time = elapsed_seconds.count();
    //recover graph
    start = std::chrono::system_clock::now();
    recover_the_matching(treeedge,treevertex,treevertexdegree,treedge_twin_1,treedge_twin_2,matching);
    auto end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    //std::cout <<  "elapsed time: " << elapsed_seconds.count() << "s\n";
    recover_time=elapsed_seconds.count();
    int mcount=0;
    
    for(int i=0;i<n;i++)
        if(matching[i].mate>-1)
        {
            row_match[i]=matching[i].mate - (NSIZE-m) ;
            mcount++;
        }
        else
            row_match[i]=-1;
    
    if (!is_undirected)
    {
        //copy cols
        for(int i=0;i<m;i++)
        {
            match[i]=matching[i+n].mate;
        }
    }
    
    //cleanup
    
    free(mergearray);
    free(matching);
    free(remdegree);
    free(treevertexdegree);
    free(ignore);
    treevertex.clear();
    for (int i=0;i<NSIZE;++i){
        edge[i].clear();
        treeedge[i].clear();
        treedge_twin_1[i].clear();
        treedge_twin_2[i].clear();
    }
    
}
