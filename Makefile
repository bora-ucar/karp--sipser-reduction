TARGET = expBasicKS12

INCS = -I./
LIBS = -lm
OBJS = mmio.o graphio.o basicKS2.o expBasicKS12.o
CPPFLAGS = -O3 -std=c++11 -Wall
CFLAGS = -O3 -Wall
CPP = g++-9
CCC = gcc-9

all: $(TARGET)

expBasicKS12: $(OBJS) utils.h graph.h mmio.h
	$(CPP) $(OBJS) $(LIBS) $(CPPFLAGS) -o expBasicKS12

clean:
	/bin/rm -f *.o *~ $(TARGET)

%.o : %.c
	$(CCC) $(CFLAGS) $(INCS) -c $*.c

%.o : %.cpp
	$(CPP) $(CPPFLAGS) $(INCS) -c $*.cpp

